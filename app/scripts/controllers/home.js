(function(){
  'use strict';

  /* @ngInject */
  function Controller($rootScope, $scope, ItemService) {
      this.$rootScope = $rootScope;
      this.$scope = $scope;
      this.ItemService = ItemService;
      this.$scope.itemsList = [];
      this.$scope.showNew = false;
      this.$scope.loading = true;
      this.$scope.newItemText = '';
      this.init();
  }

  Controller.prototype = {
      init: function() {
          // Binding functions to expose them on the view
          this.$scope.cancelNew = this.cancelNew.bind(this);
          this.$scope.addNewItem = this.addNewItem.bind(this);
          this.$scope.clearAll = this.clearAll.bind(this);

          this.getItems();
          this.setWatchers();
      },

      getItems: function() {
          this.ItemService.getItems(function(res) {
              this.$scope.loading = false;
              this.$scope.itemsList = res;
              this.$scope.$apply();
          }.bind(this));
      },

      removeItem: function(itemId) {
          this.ItemService.deleteItem(itemId, function() {
              this.getItems();
          }.bind(this));
      },

      editItem: function(item) {
          this.ItemService.editItem(item, function() {});
      },

      addNewItem: function() {
          var item = {
              content: this.$scope.newItemText,
              order: (this.$scope.itemsList.length + 1)
          };

          this.ItemService.createItem(item, function() {
            this.getItems();
          }.bind(this));

          // After adding the item
          // Lets go back to the default status
          this.cancelNew();
      },

      cancelNew: function() {
          this.$scope.showNew = false;
          this.$scope.newItemText = '';
      },

      editOrder: function(items) {
          items.map(function(item) {
              this.ItemService.editItem(item, function() {});
              return item;
          }.bind(this));
      },

      clearAll: function() {
          this.ItemService.clearItems(function() {
              this.getItems();
          }.bind(this));
      },

      setWatchers: function() {
          this.$rootScope.$on('refreshList', function (event) {
              this.getItems();
          }.bind(this));

          this.$rootScope.$on('deleteItem', function (event, args) {
              this.removeItem(args.itemId);
          }.bind(this));

          this.$rootScope.$on('editItem', function (event, args) {
              this.editItem(args);
          }.bind(this));

          this.$rootScope.$on('editOrder', function (event, args) {
              this.editOrder(args);
          }.bind(this));
      }

  };

  angular
      .module('sortableListApp')
      .controller('HomeController', ['$rootScope', '$scope', 'ItemService', Controller]);

  })();
