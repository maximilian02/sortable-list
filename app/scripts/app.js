'use strict';

/**
 * Main module of the application.
 */
angular
.module('sortableListApp', [
  'ngRoute',
  'ui.sortable'
])
.config(function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $routeProvider
    .when('/', {
      templateUrl: 'app/views/home.html',
      controller: 'HomeController'
    })
    .otherwise({
      redirectTo: '/'
    });
});
