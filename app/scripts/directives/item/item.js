(function() {
    'use strict';

    /* @ngInject */
    function Controller ($rootScope, $scope) {
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.$scope.editMode = false;
        this.$scope.itemContent = false;
        this.init();
    }

    Controller.prototype = {
        init: function() {
            this.$scope.editItem = this.editItem.bind(this);
            this.$scope.submitEditItem = this.submitEditItem.bind(this);
            this.$scope.cancelEdit = this.cancelEdit.bind(this);
        },

        editItem: function() {
            this.$scope.editMode = true;
            this.$scope.itemContent = this.$scope.itemData.content;
        },

        submitEditItem: function() {
            this.$scope.itemData.content = this.$scope.itemContent;
            this.$rootScope.$broadcast('editItem', {
               id: this.$scope.itemData.id,
               content: this.$scope.itemContent
            });
            this.$scope.editMode = false;
        },

        cancelEdit: function() {
            this.$scope.itemContent = '';
            this.$scope.editMode = false;
        }
    };

    /* @ngInject */
    function Directive() {
        return {
            restrict: 'E',
            controller: 'ItemController',
            scope: {
                itemData: '=',
            },
            templateUrl: 'app/views/partials/item.html'
        };
    }

    angular.module('sortableListApp')
        .controller('ItemController', ['$rootScope', '$scope', Controller])
        .directive('item', Directive);
})();
