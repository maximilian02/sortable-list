(function() {
    'use strict';

    /* @ngInject */
    function Controller ($rootScope, $scope) {
        this.$rootScope = $rootScope;
        this.$scope = $scope;
        this.$scope.sortableOut = false;
        this.init();
    }

    Controller.prototype = {
        init: function() {
            this.$scope.sortableOptions = {
                stop: this.stop.bind(this),
                receive: this.receive.bind(this),
                over: this.over.bind(this),
                out: this.out.bind(this),
                beforeStop: this.beforeStop.bind(this),
            }
            this.$scope.loading = false;
        },

        stop: function(e, ui) {
            for (var i = 0, len = this.$scope.itemsList.length; i < len; i++) {
                if(this.$scope.itemsList[i].order !== (i + 1)) {
                  this.$scope.itemsList[i].order = (i + 1);
                }
            }
            this.$rootScope.$broadcast('editOrder', this.$scope.itemsList);
        },

        receive: function(e, ui) { this.$scope.sortableOut = false; },
        over: function(e, ui) { this.$scope.sortableOut = false },
        out: function(e, ui) { this.$scope.sortableOut = true; },

        beforeStop: function(e, ui) {
           if (this.$scope.sortableOut) {
              // Kinda weird way of getting the itemId
              // maybe exists something better or cleaner... anyways.
              var itemId = ui.item.context.attributes['data-id'].value;
              this.$rootScope.$broadcast('deleteItem', {
                  itemId: parseInt(itemId)
              });
           }
        }

    };

    /* @ngInject */
    function Directive() {
        return {
            restrict: 'E',
            controller: 'ListController',
            scope: {
                itemsList: '=',
                loading: '='
            },
            templateUrl: 'app/views/partials/list.html'
        };
    }

    angular.module('sortableListApp')
        .controller('ListController', ['$rootScope', '$scope', Controller])
        .directive('list', Directive);
})();
