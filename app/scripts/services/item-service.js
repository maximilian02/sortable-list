(function() {
    'use strict';

    function Service(DataService) {
        this.DataService = DataService;
    }

    Service.prototype = {

        getItems: function(callback) {
            return this.DataService.get(callback);
        },

        createItem: function(item, callback) {
            this.DataService.create(item, callback);
        },

        editItem: function(item, callback) {
            this.DataService.update(item, callback);
        },

        deleteItem: function(itemId, callback) {
            this.DataService.delete(itemId, callback);
        },

        clearItems: function(callback) {
            this.DataService.clear(callback);
        }
    };

    angular
      .module('sortableListApp')
      .service('ItemService', ['DataService', Service]);

  })();
