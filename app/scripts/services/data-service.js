(function() {
    'use strict';

    function Service() {
        this.db = new Dexie('items');

        // Define the simple item schema
        this.db.version(1)
        		.stores({
                items: '++id,content,order'
        		});

        // Open the magic box
      	this.db.open()
        		.catch(function(error){
                alert('Uh oh : ' + error);
        		});
    }

    Service.prototype = {

        executeRequest: function(promise, callback) {
            this.resolveRequest(promise, callback);
            return promise;
        },

        get: function (callback) {
          var promise = this.db.items
              .orderBy('order')
              .toArray();

          return this.executeRequest(promise, callback);
        },

        create: function (data, callback) {
            var promise = this.db.items.add(data);
            return this.executeRequest(promise, callback);
        },

        update: function (item, callback) {
            var promise = this.db.items
                .update(item.id, item);
            return this.executeRequest(promise, callback);
        },

        delete: function (itemId, callback) {
            var promise = this.db.items
                .delete(itemId);
            return this.executeRequest(promise, callback);
        },

        clear: function (callback) {
            var promise = this.db.items
                .clear();
            return this.executeRequest(promise, callback);
        },

        resolveRequest: function(promise, callback) {
            var sanitizedCallback = typeof callback === "function" ? callback : function() {};
            promise.then(function(response) {
                sanitizedCallback(response, null);
                return response;
            }, function(error) {
                sanitizedCallback(null, error);
                return error;
            }).finally(function() {
                this.db.close();
            });
        }

    };

    angular
      .module('sortableListApp')
      .service('DataService', [Service]);

  })();
