# README #

This is a sortable list using sortable ui for AngularJS.

Feel free to use as you need.

### How do I get set up? ###

I've used the npm http-server module to run it, but feel free to use what you want.

If you want to use [http-server](https://www.npmjs.com/package/http-server) just run: npm install http-server -g

* Clone de repo
* Move to the folder and run: bower install
* After all bower modules are downloaded run: http-server

And that's it, you can access through the browser as always: http://localhost:8080 or http://127.0.0.1:8080

### Who do I talk to? ###

* Max - [maxzelarayan.com](http://maxzelarayan.com/)